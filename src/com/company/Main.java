package com.company;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class Main {

    private static final int numbers = 10;
    private static final boolean permutation = true;
    private static int sum = 100;

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        Scanner sc = new Scanner(System.in);
        int threats=1;

        while (true) {

            System.out.println("Uruchomić program sekwencyjnie? T/N");

            String answer= sc.next();

            if (answer.equals("T") ||  answer.equals("t")){

                break;

            }

            else if(answer.equals("N") || answer.equals("n")) {

                System.out.println("Na ilu watkach uruchomic program?");

                while (!sc.hasNextInt()) {

                    System.out.println("To nie jest liczba! Podaj liczbę.");
                    sc.next();
                }

                threats = sc.nextInt();
                break;

            }
            else{

                System.out.println("Proszę wybrac jedną z dwóch opcji T/N");

            }
        }
        //Configure how many threads to run
        ExecutorService service = Executors.newFixedThreadPool(threats);
        List<Future<Integer>> results = new ArrayList<>();

        long startTime = System.nanoTime();

        for (int i = 0; i < numbers; i++) {
            for (int j = 0; j < numbers; j++) {

                if (i == j) continue;

                int[] temp = new int[10];

                temp[0] = i;
                temp[1] = j;

                int index = 2;

                for (int k = 0; k < numbers; k++) {

                    if (k == i || k == j) continue;

                    temp[index] = k;
                    index++;

                }

                //System.out.println(Arrays.toString(temp));
                Future<Integer> result = service.submit(() -> {
                    int counter = 0;
                    int firstNumber = temp[0];
                    int secondNumber = temp[1];
                    do {
                        counter += checkMatchingPermutations(temp);
                    } while (nextPermutation(temp) && firstNumber == temp[0] && secondNumber == temp[1]);
                    return counter;
                });
                results.add(result);

            }
        }

        service.shutdown();

        int finalCounter = 0;
        for (Future<Integer> result : results) {
            finalCounter += result.get();
        }
        finalCounter *= 2;
        System.out.println("Liczba sposobów: " + finalCounter);


        long endTime = System.nanoTime();

        // get difference of two nanoTime values
        long timeElapsed = endTime - startTime;
        System.out.println("Czas działania : " + timeElapsed / 1000000000.0);

    }
    //generating all possibilities
    static boolean nextPermutation(int[] array) {

        int i = array.length - 1;

        while (i > 0 && array[i - 1] >= array[i]) {

            i--;

        }

        if (i <= 0) {

            return false;

        }

        int j = array.length - 1;

        while (array[j] <= array[i - 1]) {

            j--;

        }

        int temp = array[i - 1];

        array[i - 1] = array[j];
        array[j] = temp;

        j = array.length - 1;

        while (i < j) {

            temp = array[i];
            array[i] = array[j];
            array[j] = temp;

            i++;
            j--;

        }

        return true;
    }

    //Checking possibilities
    static int checkMatchingPermutations(int[] arr) {

        int i1, i2, i3;
        int counter = 0;

        for (int i = 1; i <= numbers - 2; i++) {

            for (int j = 1; j <= numbers - 1 - i; j++) {

                if ((j == 1 && arr[i + j - 1] == 0)) {

                    continue;
                }

                i1 = i2 = i3 = 0;

                for (int l = 0; l < numbers; l++) {

                    if (l < i){

                        i1 = 10 * i1 + arr[l];

                    }
                    else if (l < i + j) {

                        i2 = 10 * i2 + arr[l];

                    }
                    else{

                        i3 = 10 * i3 + arr[l];

                    }
                }

                if ((i1 / i2 + i3) == sum && i1 % i2 == 0) {

                    counter++;

                    if (permutation){

                        System.out.println(i1 + "/" + i2 + "+" + i3);

                    }

                }
            }
        }

        return counter;

    }
}

